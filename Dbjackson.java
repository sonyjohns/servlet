import java.io.*;
import java.io.PrintWriter;
import java.util.ResourceBundle;
import org.json.*;
import java.util.*;
import java.lang.*;
import java.sql.*;

import javax.servlet.*;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.fasterxml.jackson.datatype.jsonorg.*;
import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;
public class Dbjackson extends HttpServlet {

	 @Override
    public void doPost(HttpServletRequest request,
                      HttpServletResponse response)
        throws IOException, ServletException
   {
       response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
       PrintWriter out = response.getWriter();
       ObjectMapper mapper = new ObjectMapper();
       mapper.registerModule(new JsonOrgModule());
        String dbName = "jdbc:postgresql://localhost/training";
         String dbDriver = "org.postgresql.Driver";
         String userName = "postgres";
         String password = "sonynew123"; 
         		 Connection con=null;
          try{
         Class.forName(dbDriver);
          con = DriverManager.getConnection(dbName, userName, password);
         System.out.println("Got Connection");
         Statement statement = con.createStatement();
         String sql = "select lname from mytable";
         ResultSet rs = statement.executeQuery(sql);
        // StringWriter str = new StringWriter();
         String s=null;
       //  out.println("[");
        // int i=0;
          JSONArray arr = new JSONArray();
          while (rs.next()) {
            //  Details obj = mapper.readValue(rs.getString("lname"),Details.class); 
          		 JSONObject jo = mapper.readValue(rs.getString("lname"),JSONObject.class); 
            //  mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
           //  s=  mapper.writeValueAsString(jo);
           //  JSONObject og =new JSONObject(jo);
             arr.put(jo);

             
          //   if(i<1)
           //  {
           //  	 out.println(",");
            // i++;
         }
         out.println(arr);
      //  out.println("]");
          
     }
     catch(SQLException e)
     {
     	e.printStackTrace();
     }
     catch(ClassNotFoundException e)
     {
     	e.printStackTrace();
     }
     catch(JsonMappingException e)
     {
          e.printStackTrace();
     }
     catch(JsonGenerationException e)
     {
     	e.printStackTrace();
     }
   }
 }    
  class Details
     {
     	private List<String> address;
     	public List<String> getAddress()
     	{
     		return address;
     	}
     	public void setAddress(List<String> address)
     	{
         	this.address=address;
     	}
     	public String toString()
     	{
     		String s="Address="+Arrays.toString(getAddress().toArray());
     		return s;
     	}
     
     }