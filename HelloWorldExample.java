import java.io.*;
import java.io.PrintWriter;
import java.util.ResourceBundle;
import org.json.*;
import java.util.*;
import java.lang.*;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;


import javax.servlet.ServletException;
import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HelloWorldExample extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    public void doPost(HttpServletRequest request,
                      HttpServletResponse response)
        throws IOException, ServletException
    {
    try
    {
       response.setContentType("image/jpeg");
        response.setCharacterEncoding("UTF-8");
	ServletContext cntx= request.getServletContext();
	String filename = cntx.getRealPath("/jsp/jsp2/jspx/textRotate.jpg");
	 File file = new File(filename);
	response.setContentLength((int)file.length());
	 FileInputStream in = new FileInputStream(file);
      OutputStream out = response.getOutputStream();
	 byte[] buf = new byte[1024];
       int count = 0;
       while ((count = in.read(buf)) >= 0) {
         out.write(buf, 0, count);
      }
    out.close();
    in.close();
	}
	catch(Exception e)
	{
		System.out.println("exception occccuress"+e.getMessage());
	}
	}
}